package com.practice.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.practice.demo.dto.Currencies;
import com.practice.soap.GetInfoRequestBody;
import com.practice.soap.GetInfoResponseBody;
import com.practice.soap.ObjectFactory;
import com.practice.soap.SoapClient;
import com.practice.soap.dto.MNBCurrencies;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.xml.bind.JAXBElement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTest {

    @Mock
    private SoapClient soapClient;

    @Mock
    private MNBMapper mnbMapper;

    @InjectMocks
    private CurrencyService underTest;

    @Test
    public void getCurrenciesInfoTest() throws JsonProcessingException {
//       ObjectFactory objectFactory = new ObjectFactory();
//        //TODO
//        GetInfoRequestBody infoRequestBody = objectFactory.createGetInfoRequestBody();
//        when(soapClient.getInfo()).thenReturn(infoRequestBody);
//
//        //TODO
//        when(mnbMapper.mapCurrenciesInfo(any())).thenReturn(null);
//
//        Currencies currenciesInfo = underTest.getCurrenciesInfo();
//
//        assertThat(currenciesInfo.getCurrencies().get(0)).isEqualTo("CYN");



//        GetInfoResponseBody infoResponseBody = new GetInfoResponseBody();
//        objectFactory.createGetInfoResponseBody();
//
//        when(soapClient.getInfo()).thenReturn(infoResponseBody);

//        XmlMapper xmlMapper = new XmlMapper();
//        MNBCurrencies mnbCurrencies = xmlMapper.readValue(soapClient.getInfo().getGetInfoResult().getValue(), MNBCurrencies.class);

//        when(mnbMapper.mapCurrenciesInfo(any())).thenReturn(Currencies.builder().build());
//
//        Currencies currenciesInfo = underTest.getCurrenciesInfo();
//
//        assertThat(currenciesInfo.getCurrencies().get(0)).isEqualTo("HUF");

    }
}
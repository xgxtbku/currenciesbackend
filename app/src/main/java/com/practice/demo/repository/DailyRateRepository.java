package com.practice.demo.repository;

import com.practice.demo.model.DailyRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DailyRateRepository extends JpaRepository<DailyRate, Long> {

}

package com.practice.demo.controller;


import com.practice.demo.model.HelloModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@RestController
public class HelloController {

    private static final String template = "Hellohi, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping(value = "/hello", produces = {MediaType.APPLICATION_JSON_VALUE})
    public HelloModel hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        log.info("Hello method called");
        return HelloModel.builder()
                .id(counter.incrementAndGet())
                .content(String.format(template, name))
                .build();
    }
}

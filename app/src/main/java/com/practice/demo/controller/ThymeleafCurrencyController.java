package com.practice.demo.controller;

import com.practice.demo.dto.Currencies;
import com.practice.demo.dto.ExchangeRates;
import com.practice.demo.service.CurrencyService;
import com.practice.demo.service.PdfGeneratorService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/pdf")
public class ThymeleafCurrencyController {


    private final CurrencyService currencyService;
    private final PdfGeneratorService pdfGeneratorService;

    @RequestMapping(path = "/info")
    public ResponseEntity<?> getInfoPdf() throws IOException {

        Currencies info = currencyService.getCurrenciesInfo();
        byte[] bytes = pdfGeneratorService.getPdfContent("info", "info", info);

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(bytes);
    }

    @RequestMapping(path = "/exchangeRates")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> getExchangeRatesPdf(@RequestParam(value = "startDate") String startDate,
                                                 @RequestParam(value = "endDate") String endDate,
                                                 @RequestParam(value = "curr") List<String> curr) throws IOException {

        ExchangeRates exchangeRates = currencyService.getExchangeRates(startDate, endDate, curr);
        byte[] bytes = pdfGeneratorService.getPdfContent("exchangeRates", "exchangeRates", exchangeRates);

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(bytes);
    }
}

package com.practice.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.practice.demo.dto.Currencies;
import com.practice.demo.dto.CurrentExchangeRates;
import com.practice.demo.dto.ExchangeRates;
import com.practice.demo.model.History;
import com.practice.demo.repository.HistoryRepository;
import com.practice.demo.service.CurrencyService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/currencies")
public class CurrencyController {

    private final CurrencyService currencyService;

    private final HistoryRepository historyRepository;

    @GetMapping(value = "/info", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Getting currencies info",
            notes = "It retrieves the time interval that can be queried and lists all queriable currencies.",
            response = Currencies.class)
    public Currencies getInfo() throws JsonProcessingException {
        log.info("GetInfo method called");
        return currencyService.getCurrenciesInfo();
    }

    @GetMapping(value = "/currentExchangeRates", produces = {MediaType.APPLICATION_JSON_VALUE})
    public CurrentExchangeRates getCurrentExchangeRates() throws JsonProcessingException {
        log.info("GetCurrentExchangeRates method called");
        return currencyService.getCurrentExchangeRates();
    }

    @GetMapping(value = "/exchangeRates", produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('USER')")
    public ExchangeRates getExchangeRates(
            @RequestParam(required = false) String startDateInput,
            @RequestParam(required = false) String endDateInput,
            @RequestParam List<String> currencyList) throws JsonProcessingException {
        log.info("GetExchangeRates method called");

        persistUserHistoryToDatabase(startDateInput, endDateInput, currencyList);

        return currencyService.getExchangeRates(startDateInput, endDateInput, currencyList);
    }

    private void persistUserHistoryToDatabase(String startDateInput, String endDateInput, List<String> currencyList) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {

            String currentUserName = authentication.getName();
            String currencies = String.join(", ", currencyList);


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

            historyRepository
                    .save(new History(currentUserName, simpleDateFormat.format(timestamp), currencies, startDateInput, endDateInput));

        }
    }


}

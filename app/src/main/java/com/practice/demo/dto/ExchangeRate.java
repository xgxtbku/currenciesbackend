package com.practice.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ExchangeRate {

    String unit;
    String currency;
    List<DailyValue> dailyValues;
}

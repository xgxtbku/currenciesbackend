package com.practice.demo.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HelloModel {

    private final long id;
    private final String content;

}

package com.practice.demo.service;

import com.itextpdf.html2pdf.HtmlConverter;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.ByteArrayOutputStream;

@AllArgsConstructor
@Service
public class PdfGeneratorService {

    private final TemplateEngine templateEngine;

    public byte[] getPdfContent(String propertyName, String templateName, Object data) {

        Context context = new Context();
        context.setVariable(propertyName, data);
        String infoHtml = templateEngine.process(templateName, context);

        ByteArrayOutputStream target = new ByteArrayOutputStream();

        HtmlConverter.convertToPdf(infoHtml, target);

        return target.toByteArray();
    }
}

package com.practice.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.practice.demo.config.CacheConfig;
import com.practice.demo.dto.Currencies;
import com.practice.demo.dto.CurrentExchangeRates;
import com.practice.demo.dto.ExchangeRates;
import com.practice.soap.SoapClient;
import com.practice.soap.dto.MNBCurrencies;
import com.practice.soap.dto.MNBCurrentExchangeRates;
import com.practice.soap.dto.MNBExchangeRates;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class CurrencyService {

    private final SoapClient soapClient;
    private final MNBMapper mnbMapper;

    @Cacheable(value = CacheConfig.CACHE_CURRENCY_INFO)
    public Currencies getCurrenciesInfo() throws JsonProcessingException {

        String xmlString = soapClient.getInfo().getGetInfoResult().getValue();
        XmlMapper xmlMapper = new XmlMapper();
        MNBCurrencies mnbCurrencies = xmlMapper.readValue(xmlString, MNBCurrencies.class);

        return mnbMapper.mapCurrenciesInfo(mnbCurrencies);
    }

    public CurrentExchangeRates getCurrentExchangeRates() throws JsonProcessingException {

        String xmlString = soapClient.getCurrentExchangeRates().getGetCurrentExchangeRatesResult().getValue();
        XmlMapper xmlMapper = new XmlMapper();
        MNBCurrentExchangeRates mnbCurrentExchangeRates = xmlMapper.readValue(xmlString, MNBCurrentExchangeRates.class);

        return mnbMapper.currentExchangeRates(mnbCurrentExchangeRates);
    }

    @Cacheable(value = CacheConfig.CACHE_CURRENCY_RATES)
    public ExchangeRates getExchangeRates(String startDateInput, String endDateInput, List<String> currencyList) throws JsonProcessingException {

        log.info("getExchangeRates called.");

        String xmlString = soapClient.getExchangeRates(startDateInput, endDateInput, currencyList).getGetExchangeRatesResult().getValue();
        // log.info("xml: " + xmlString);
        XmlMapper xmlMapper = new XmlMapper();
        MNBExchangeRates mnbExchangeRates = xmlMapper.readValue(xmlString, MNBExchangeRates.class);
        // log.info("java-mnb-obj: " + mnbExchangeRates);

        return mnbMapper.exchangeRates(mnbExchangeRates);
    }
}

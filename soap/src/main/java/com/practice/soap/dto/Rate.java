package com.practice.soap.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Data;

@Data
public class Rate {

    @JacksonXmlProperty(isAttribute = true)
    String unit;
    @JacksonXmlProperty(isAttribute = true)
    String curr;
    @JacksonXmlText
    String value;
}

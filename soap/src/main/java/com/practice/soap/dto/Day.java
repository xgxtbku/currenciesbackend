package com.practice.soap.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.List;

@Data
public class Day {

    @JacksonXmlProperty(isAttribute = true)
    public String date;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Rate")
    public List<Rate> rate;

}
